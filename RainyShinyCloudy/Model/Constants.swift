//  Constants.swift
//  RainyShinyCloudy
//
//  Created by Mark Davidson on 2/9/17.
//  Copyright © 2017 Mark Davidson. All rights reserved.

import Foundation

let BASE_URL = "http://samples.openweathermap.org/data/2.5/weather?"
let LATITUDE = "lat="
let LONGITUDE = "&lon="
let APP_ID = "&appid="
let API_KEY = "346366d4fe8abfee0c12e4491e561650"

typealias DownloadComplete = () -> ()

let CURRENT_WEATHER_URL = "\(BASE_URL)\(LATITUDE)\(LONGITUDE)\(APP_ID)\(API_KEY)"


