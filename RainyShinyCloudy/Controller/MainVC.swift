//  MainVC.swift
//  RainyShinyCloudy
//
//  Created by Mark Davidson on 1/9/2017.
//  Copyright © 2017 Mark Davidson. All rights reserved.

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var currentWeatherTypeLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var currentWeather = CurrentWeather();
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self;
        tableView.dataSource = self;
        
        currentWeather.downloadWeatherDetails {
            //Setup UI to load downloaded data
            
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath)
        
        return cell;
    }
    


}

